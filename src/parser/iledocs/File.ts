export interface File {
    name: string;
    description?: string;
}
