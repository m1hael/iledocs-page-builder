export interface Revision {
    date?: string;
    author?: string;
    description?: string;
}
