export enum Tag {
    Author = "author",
    Brief = "brief",
    Date = "date",
    Deprecated = "deprecated",
    Example = "example",
    Info = "info",
    Link = "link",
    Parameter = "param",
    Project = "project",
    Return = "return",
    Revision = "rev",
    Throws = "throws",
    Version = "version",
    Warning = "warning"
}
