export interface EscapeMessage {
    id: string;
    description?: string;
}
