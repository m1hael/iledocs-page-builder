import {AstSymbolPosition} from "./AstSymbolPosition";

export interface AstSymbol {
    name: string;
    position: AstSymbolPosition;
}
