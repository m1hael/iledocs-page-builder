export class SymbolDataType {
    type: string = "unknown";
    like: any;
    length: string;
    dataStructure: boolean = false;
}
