export interface AstSymbolPosition {
    line: number;
    column: number;
}
